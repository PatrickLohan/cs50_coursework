# Django

## Setup and Initial Start

### 1. Create folder for project and move to it
``` Bash
$ mkdir myDjangoProject && cd $_
```

### 2. Create and activate virtual environment
``` Bash
$ python -m venv .venv && source .venv/bin/activate
```
Make sure the venv shows somewhere on the command prompt.

### 3. Install Django
``` Bash
$ pip install Django
```

### 4. Create the project within root project folder
``` Bash
$ django-admin startproject myDjangoProject
```

### 5. Start the dev server
```Bash
$ python manage.py runserver
```

### 6. Create this first project application
```Bash
$ python manage.py startapp hello
```

### 7. Add path to urls
In myDjangoProject/myDjangProject/settings.py add `hello` to `INSTALLED_APPS` list
```python
INSTALLED_APPS = [
    'hello',
    ...
]
```