import sys

numbers = [3,6,7,2,7,9,0]

if 0 in numbers:
    print("Found!")
    sys.exit(0)

print("Not found")
sys.exit(1)
