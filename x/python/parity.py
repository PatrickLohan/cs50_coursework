"""Determine whether a number is even or odd."""
try:
    n = int(input("What is the number? "))
except ValueError:
    print("That is not a number!")
    exit()

if n % 2 == 0:
    print("That number is even!")
else:
    print("That number is odd!")
