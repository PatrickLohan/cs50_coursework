"""Get a number input."""
try:
    value_x = int(input("Number x: "))
except ValueError:
    print("That is not an int!")
    exit()
try:
    value_y = int(input("Number x: "))
except ValueError:
    print("That is not an int!")
    exit()
print(f"Added: {value_x + value_y}")
print(f"Divided: {value_x / value_y}")
print(f"Divided without typecasting to float: {value_x // value_y}")
print(f"Divided with 50 significant digits (note floating point imprecision): {(value_x / value_y):.50f}")
