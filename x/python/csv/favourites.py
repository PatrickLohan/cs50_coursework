import csv
from collections import Counter

with open('favourites.csv', 'r') as file:
    reader = csv.DictReader(file)
    counts = Counter()

    for row in reader:
        favourite = row['problem']
        counts[favourite] += 1

# for favourite, count in counts.most_common():
#     print(f'{favourite}, {count}')

favourite = input('What problem would you like to check the score of? ')
print(f"{favourite}: {counts[favourite]}")
