import csv
import sys


def main():

    # TODO: Check for command-line usage
    if len(sys.argv) != 3:
        print("Usage: python dna.py data.csv sequence.txt")
        sys.exit()

    # # TODO: Read database file into a variable
    suspects = []
    with open(sys.argv[1], "r") as suspects_file:
        dctRdr = csv.DictReader(suspects_file)
        subsequences = dctRdr.fieldnames
        reader = dctRdr
        for row in reader:
            suspects.append(row)

    # # TODO: Read DNA sequence file into a variable
    with open(sys.argv[2], "r") as sequence_file:
        sequence = sequence_file.read()

    # # TODO: Find longest match of each STR in DNA sequence
    str_counts = []
    for subsequence in subsequences[1:]:
        str_counts.append(str(longest_match(sequence, subsequence)))

    # todo: check database for matching profiles
    for suspect in suspects:
        suspect_strs = []
        for key in suspect:
            suspect_strs.append(suspect[key])
        suspect_strs.pop(0)
        if str_counts == suspect_strs:
            print(f"{suspect['name']}")
            sys.exit(0)

    print("No match")
    sys.exit(0)


def longest_match(sequence, subsequence):
    """Returns length of longest run of subsequence in sequence."""

    # Initialize variables
    longest_run = 0
    subsequence_length = len(subsequence)
    sequence_length = len(sequence)

    # Check each character in sequence for most consecutive runs of subsequence
    for i in range(sequence_length):

        # Initialize count of consecutive runs
        count = 0

        # Check for a subsequence match in a "substring" (a subset of
        # characters) within sequence
        # If a match, move substring to next potential match in sequence
        # Continue moving substring and checking for matches until out of consecutive matches
        while True:

            # Adjust substring start and end
            start = i + count * subsequence_length
            end = start + subsequence_length

            # If there is a match in the substring
            if sequence[start:end] == subsequence:
                count += 1

            # If there is no match in the substring
            else:
                break

        # Update most consecutive matches found
        longest_run = max(longest_run, count)

    # After checking for runs at each character in seqeuence, return longest run found
    return longest_run


main()
