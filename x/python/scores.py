"""Throwback to scores c program."""
from cs50 import get_int

scores = []
for i in range(3):
    score = get_int("Score: ")
#    scores = scores + score
    scores.append(score)

average = sum(scores) / len(scores)

print(f"Average score = {average}")
