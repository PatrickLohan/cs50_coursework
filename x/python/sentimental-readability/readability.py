"""Computes approximate grade level needed to comprehend a piece of text."""
from cs50 import get_string


def main():
    letter_count = 0
    word_count = 1
    sentence_count = 0
    txt = get_string("Text: ").strip()

    for i in range(len(txt)):
        if txt[i].isalpha():
            letter_count += 1

        if txt[i].isspace():
            word_count += 1

        if txt[i] == "." or txt[i] == "?" or txt[i] == "!":
            sentence_count += 1

    L = float(letter_count / word_count * 100)
    S = float(sentence_count / word_count * 100)
    cl_index = 0.0588 * L - 0.296 * S - 15.8

    if cl_index < 1:
        print("Before Grade 1")
    elif cl_index > 15:
        print("Grade 16+")
    else:
        print(f"Grade: {round(cl_index)}")


if __name__ == "__main__":
    main()
