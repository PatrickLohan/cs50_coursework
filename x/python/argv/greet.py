from sys import argv

if len(argv) == 2:  # argv ignores the python part of (e.g.) `python greet.py Apollo`
    print(f"Hello, {argv[1]}!")
elif len(argv) < 2:
    print("Hello, world!")
else:
    # [1:] means range starting at 1th index
    # [:-1] means range up to (but not including) -1th (last) index
    for arg in argv[1:]:
        print(arg)
