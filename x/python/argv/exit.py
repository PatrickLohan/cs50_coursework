from sys import argv
from sys import exit

if len(argv) != 2:
    print("Missing arg")
    exit(1)

print(f"Hello, {argv[1]}")
exit(0)
