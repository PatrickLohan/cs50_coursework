"""Credit card number validator."""
import re
import cs50

"""
This program checks a user inputted number to see if it is a valid credit
card number. To do that it uses Luhn's algorithm.
"""


def main():
    """Yada yada."""
    card_number = cs50.get_int("Number: ")
    if luhns_algo(card_number):
        print(match_type(str(card_number)))
    else:
        print("INVALID")


def luhns_algo(n):
    """Use Luhn's algorithm."""
    num_list = list(map(int, str((n))))
    result = num_list.pop()
    num_list.reverse()

    for i, v in enumerate(num_list):
        if i % 2 == 0:
            if (v * 2) > 9:
                result += (v * 2) - 9
            else:
                result += v * 2
        else:
            result += v
    if result % 10 == 0:
        return True


def match_type(ccn):
    cards = [
        {"name": "VISA", "match_regex": "^[4]", "length": [13, 16]},
        {"name": "AMEX", "match_regex": "^[34|37]", "length": [15]},
        {"name": "MASTERCARD", "match_regex": "^[51-55]", "length": [16]},
    ]
    for card in cards:
        c = re.compile(card["match_regex"])
        m = c.match(ccn)
        print(f"Card: {card['name']}")
        if m and len(ccn) in card["length"]:
            return card["name"]
    return "INVALID"


if __name__ == "__main__":
    main()
