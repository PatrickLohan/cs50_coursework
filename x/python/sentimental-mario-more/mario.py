"""Simulate simple mario block pyramid using Python."""


def main():
    """Create procedure."""
    height = get_input()
    build_pyramid(height)


def get_input():
    """Ask user for the height."""
    n = 0
    while n < 1 or n > 8:
        try:
            n = int(input("Height: "))
        except ValueError:
            n = int(input("Height: "))
    return n


def build_pyramid(height):
    """Create pyramid by iterating through each row, and each character in each row."""
    for row in range(0, height):
        # Left wall
        for left_brick in range(height, 0, -1):
            if left_brick <= (row + 1):
                print("#", end="")
            else:
                print(" ", end="")
        # Atrium
        print("  ", end="")
        # Right wall
        for r_brick in range(0, height):
            if row >= (r_brick):
                print("#", end="")
            else:
                print("", end="")
        print("")


if __name__ == "__main__":
    main()
