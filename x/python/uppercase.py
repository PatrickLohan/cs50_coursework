"""Throwback to uppercase c program."""
from cs50 import get_string

before = get_string("Before: ")
after = before.upper()

print(f"After:  {after}")
