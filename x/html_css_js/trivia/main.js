let resultText = " ";
let boxColor = "";
let btnColor = "";

const swallowBtns = document.querySelectorAll(".swallow-btn");
swallowBtns.forEach(btn =>{
  btn.addEventListener("click", (e) => {
    document.querySelector("#swallow-result").innerHTML = "";
    document.querySelector(`#${btn.id}`).style.backgroundColor = "white";

    if(btn.id != "btn3"){
      document.querySelector("#swallow-result").innerHTML = "Incorrect";
      document.querySelector(`#${btn.id}`).style.backgroundColor = "red";
    } else {
      document.querySelector("#swallow-result").innerHTML = "Correct";
      document.querySelector("#btn3").style.backgroundColor = "green";
    }
    e.preventDefault();
  })
})


document.querySelector("form").addEventListener("submit", (e) => {
  let resultText = " ";
  let boxColor = "";

  let input = document.querySelector("input").value;
  if(input == "We found them!"){
    boxColor = "green";
    resultText = "Correct!";
  } else {
    boxColor = "red";
    resultText = "Incorrect";
  }
  document.querySelector("#coco-result").innerHTML = resultText;
  document.querySelector("input").style.backgroundColor = boxColor;
  e.preventDefault();
});
