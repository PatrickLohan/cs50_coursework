To get some sense of an answer to the first question, I have written:
``` SQL
SELECT AVG((energy + valence + danceability) / 3) FROM songs;
```
RESULT: 0.6199876666666667

One way in which this may not be representative of the listener is that the
number of times a track has been played is not present in the database table. For
example the listener could have played one track a thousand times and all the
other tracks just once each. So the calculation is too general. A better way
would, of course, be to add a column for number of times a track has been played
and give some weighting to more plays.