-- Keep a log of any SQL queries you execute as you solve the mystery.
-- 1. I want to get a clear picture of the facts so I will look at more hard
-- evidence first before interviews
SELECT description FROM crime_scene_reports
WHERE month = 7 AND day = 28 AND description LIKE "%duck%";
-- description
-- ------------------------------------------------------------
-- Theft of the CS50 duck took place at 10:15am at the Humphrey
--  Street bakery. Interviews were conducted today with three w
-- itnesses who were present at the time – each of their interv
-- iew transcripts mentions the bakery.

-- 2. Now we will check in on the interviews
SELECT name,transcript FROM interviews
WHERE transcript LIKE "%bakery%" AND month = 7 AND day = 28;
-- name     transcript
-- -------  ------------------------------------------------------------
-- Ruth     Sometime within ten minutes of the theft, I saw the thief ge
--          t into a car in the bakery parking lot and drive away. If yo
--          u have security footage from the bakery parking lot, you mig
--          ht want to look for cars that left the parking lot in that t
--          ime frame.
--
-- Eugene   I don't know the thief's name, but it was someone I recogniz
--          ed. Earlier this morning, before I arrived at Emma's bakery,
--           I was walking by the ATM on Leggett Street and saw the thie
--          f there withdrawing some money.
--
-- Raymond  As the thief was leaving the bakery, they called someone who
--           talked to them for less than a minute. In the call, I heard
--           the thief say that they were planning to take the earliest
--          flight out of Fiftyville tomorrow. The thief then asked the
--          person on the other end of the phone to purchase the flight
--          ticket.

-- Facts:
-- (3) Thief withdrew cash at ATM on Leggett Street
--       - TABLES:
--         - atm_transactions
--         - bank_accounts
--         - people
-- (4) Car leaves bakery between 1015hrs and 1025hrs
--       - TABLES:
--         - bakery_security_logs
--         - people
-- (5) Thief called accomplice as leaving bakery:
--       - call duration < 60 seconds
--       - plan to take earliest flight
--       - accomplice to buy ticket
--       - TABLES:
--         - phone_calls


-- 3. ATM transactions on Leggett Street on 28/7
SELECT
people.name
FROM atm_transactions,bank_accounts,people
WHERE bank_accounts.person_id = people.id
AND bank_accounts.account_number = atm_transactions.account_number
AND atm_transactions.atm_location = 'Leggett Street'
AND atm_transactions.year = 2021
AND atm_transactions.month = 7
AND atm_transactions.day = 28
AND atm_transactions.transaction_type = 'withdraw';
-- OUTPUT:
-- name
-- -------
-- Bruce
-- Diana
-- Brooke
-- Kenny
-- Iman
-- Luca
-- Taylor
-- Benista

-- 4. So the next place I will look is in the bakery_security_logs. We hope
-- to see a person with the same license plate leaving the bakery within 10
-- minutes after 1015hrs who also withdrew money per (3).
SELECT people.name
FROM bakery_security_logs,people
WHERE bakery_security_logs.license_plate = people.license_plate
AND bakery_security_logs.year = 2021
AND bakery_security_logs.month = 7
AND bakery_security_logs.day = 28
AND bakery_security_logs.hour = 10
AND bakery_security_logs.minute BETWEEN 15 AND 25
AND bakery_security_logs.activity = "exit"
ORDER BY hour,minute;
-- OUTPUT:
-- name
-- -------
-- Vanessa
-- Bruce
-- Barry
-- Luca
-- Sofia
-- Iman
-- Diana
-- Kelsey

-- 5. Phone calls of less than a minute.
SELECT name FROM phone_calls,people
WHERE phone_calls.caller = people.phone_number
AND year= 2021 AND month = 7 AND day = 28
AND duration < 60;
-- OUTPUT:
-- name
-- -------
-- Sofia
-- Kelsey
-- Bruce
-- Kelsey
-- Taylor
-- Diana
-- Carina
-- Kenny
-- Benista

-- 6. Let's now correlate the three outputs to see who is in all of them.
SELECT name FROM people
WHERE name IN (
SELECT people.name
FROM atm_transactions,bank_accounts,people
WHERE bank_accounts.person_id = people.id
AND bank_accounts.account_number = atm_transactions.account_number
AND atm_transactions.atm_location = 'Leggett Street'
AND atm_transactions.year = 2021
AND atm_transactions.month = 7
AND atm_transactions.day = 28
AND atm_transactions.transaction_type = 'withdraw'
) AND name IN (
SELECT people.name
FROM bakery_security_logs,people
WHERE bakery_security_logs.license_plate = people.license_plate
AND bakery_security_logs.year = 2021
AND bakery_security_logs.month = 7
AND bakery_security_logs.day = 28
AND bakery_security_logs.hour = 10
AND bakery_security_logs.minute BETWEEN 15 AND 25
AND bakery_security_logs.activity = "exit"
) AND name IN (
SELECT name FROM phone_calls,people
WHERE phone_calls.caller = people.phone_number
AND year= 2021 AND month = 7 AND day = 28
AND duration < 60
);
-- OUTPUT:
-- name
-- -----
-- Diana
-- Bruce

-- 7. Earliest flight leaving Fiftyville 2021-07-29, and the passengers on board.
SELECT flights.id,flights.hour,flights.minute
FROM flights,airports
WHERE airports.id = flights.origin_airport_id
AND month = 7 AND DAY = 29
AND airports.city = "Fiftyville"
ORDER BY "hour" ASC, "minute" ASC
LIMIT 1;
-- OUTPUT:
-- id  hour  minute
-- --  ----  ------
-- 36  8     20

-- 8. Passengers on that plane
SELECT people.name
FROM people,passengers,flights
WHERE people.passport_number = passengers.passport_number
AND passengers.flight_id = flights.id
AND flights.id IN
(
SELECT flights.id
FROM flights,airports
WHERE airports.id = flights.origin_airport_id
AND month = 7 AND DAY = 29
AND airports.city = "Fiftyville"
ORDER BY "hour" ASC, "minute" ASC
LIMIT 1
);
-- OUTPUT:
-- name
-- ------
-- Doris
-- Sofia
-- Bruce
-- Edward
-- Kelsey
-- Taylor
-- Kenny
-- Luca

-- 9. Correlate!
SELECT flights.id, flights.hour, flights.minute, people.name
FROM flights
JOIN airports ON airports.id = flights.origin_airport_id
JOIN passengers ON passengers.flight_id = flights.id
JOIN people ON people.passport_number = passengers.passport_number
WHERE flights.year = 2021 AND flights.month = 7 AND flights.day = 29
AND passengers.passport_number IN
(SELECT DISTINCT passport_number FROM people
JOIN phone_calls ON phone_calls.caller = people.phone_number
JOIN bakery_security_logs
ON bakery_security_logs.license_plate = people.license_plate
JOIN bank_accounts ON bank_accounts.person_id = people.id
WHERE bakery_security_logs.month = 7 AND bakery_security_logs.day = 28
AND phone_calls.month = 7 AND phone_calls.day = 28
AND phone_calls.duration < 60
AND bakery_security_logs.activity = "exit"
AND bakery_security_logs.hour >= 10
)
ORDER BY flights.hour,flights.minute;

-- OUTPUT:
-- id  hour  minute  name
-- --  ----  ------  ------
-- 36  8     20      Bruce
-- 36  8     20      Taylor
-- 18  16    0       Diana

-- 10. Where does flight 36 go?
SELECT airports.city FROM airports,flights
WHERE airports.id = flights.destination_airport_id
AND year = 2021 AND month = 7 AND day = 29 AND hour = 8 and minute = 20
AND flights.destination_airport_id IN
(SELECT flights.destination_airport_id FROM airports,flights
WHERE airports.id = flights.origin_airport_id
AND airports.city = "Fiftyville"
AND year = 2021 AND month = 7 AND day = 29 AND hour = 8 and minute = 20);
-- OUTPUT:
-- city
-- -------------
-- New York City

-- 11. We need to look at who Bruce phoned the morning of the theft
SELECT people.name FROM people
JOIN phone_calls ON people.phone_number = phone_calls.receiver
WHERE year = 2021 AND month = 7 AND day = 28
AND phone_calls.duration <= 60
AND phone_calls.receiver IN
(
SELECT phone_calls.receiver FROM phone_calls
JOIN people ON people.phone_number = phone_calls.caller
WHERE people.name = "Bruce"
)
-- OUTPUT:
-- name
-- -----
-- Robin

-- ANSWERS
-- The only person present in all the logs of suspects is Bruce. He called Robin
-- and was on the ealriest flight to NYC
