SELECT
people.name,
people.phone_number,
people.passport_number,
bakery_security_logs.hour,
bakery_security_logs.minute,
bakery_security_logs.activity,
bakery_security_logs.license_plate
FROM bakery_security_logs
JOIN people ON people.license_plate = bakery_security_logs.license_plate
WHERE year = 2021 AND month = 7 AND day = 28
AND hour BETWEEN 10 AND 11
AND minute BETWEEN 15 AND 25
AND activity = "exit"
ORDER BY hour,minute
