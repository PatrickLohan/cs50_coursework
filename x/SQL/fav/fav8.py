"""WASSUP!."""
from cs50 import SQL

db = SQL("sqlite:///fav8.db")
title = input("Title: ").strip().upper()

rows = db.execute("SELECT COUNT(*) as counter FROM shows WHERE title LIKE ?", title)

row = rows[0]

print(row["counter"])
