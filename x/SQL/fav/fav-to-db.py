"""Imports titles and genres from CSV into a SQLite database."""

from cs50 import SQL as sql50
import csv

# Create database
open("fav8.db", "w").close()
db = sql50("sqlite:///fav8.db")

# Create tables
db.execute("CREATE TABLE shows (id INTEGER, title TEXT NOT NULL, PRIMARY KEY(id))")
db.execute(
    "CREATE TABLE genres (show_id INTEGER, genre TEXT NOT NULL, FOREIGN KEY(show_id) REFERENCES shows(id))"
)

# Open CSV file
with open("fav.csv", "r") as file:

    # Create DictReader
    reader = csv.DictReader(file)

    # Iterate over CSV file
    for row in reader:

        # Canoncalize title
        title = row["Name"].strip().upper()

        # Insert title
        show_id = db.execute("INSERT INTO shows (title) VALUES(?)", title)

        # Insert genres
        for genre in row["Genre"].split(", "):
            db.execute(
                "INSERT INTO genres (show_id, genre) VALUES(?, ?)", show_id, genre
            )
