"""Searches CSV for popularity of a title."""
from cs50 import SQL

# Open database
db = SQL("sqlite:///favorites8.db")

# Prompt user for title
title = input("Title: ").strip()

# Search for title
rows = db.execute("SELECT COUNT(*) AS counter FROM shows WHERE title LIKE ?", "%" + title + "%")

# Get first (and only) row
row = rows[0]

# Print popularity
print(row["counter"])
