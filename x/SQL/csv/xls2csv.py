#importing pandas as pd
import pandas as pd

# Read and store content
# of an excel file
read_file = pd.read_excel ("happy.xls")

# Write the dataframe object
# into csv file
read_file.to_csv('happy.csv')


# read csv file and convert
# into a dataframe object
df = pd.DataFrame(pd.read_csv("happy.csv"))
  
# show the dataframe 
df