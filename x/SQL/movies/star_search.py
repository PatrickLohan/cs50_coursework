from cs50 import SQL

db = SQL("sqlite:///movies.db")
query = ("SELECT * FROM movies INNER JOIN stars ON stars.movie_id = movies.id INNER JOIN people ON people.id = stars.person_id WHERE people.name = ? ORDER BY movies.year;")
person = input("Name: ").strip()

rows = db.execute(query, person)

for row in rows:
    print(f"{row['year']} {row['title']}")
