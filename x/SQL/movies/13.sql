SELECT people.name, movies.title FROM stars
JOIN people ON people.id = stars.person_id
JOIN movies ON stars.movie_id = movies.id
WHERE stars.movie_id IN
(SELECT movies.id FROM stars
JOIN movies ON movies.id = stars.movie_id
JOIN people ON stars.person_id = people.id
WHERE people.name = "Kevin Bacon"
AND people.birth = 1958)
AND people.name != "Kevin Bacon"
ORDER BY movies.year
