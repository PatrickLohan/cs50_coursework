from cs50 import SQL

db = SQL("sqlite:///movies.db")
sql = ("SELECT movies.title FROM stars JOIN movies ON movies.id = stars.movie_id JOIN people ON people.id = stars.person_id WHERE people.name = 'Johnny Depp' INTERSECT SELECT movies.title FROM stars JOIN movies ON movies.id = stars.movie_id JOIN people ON people.id = stars.person_id WHERE people.name = 'Helena Bonham Carter';")

rows = db.execute(sql)

for row in rows:
    print(row['title'])
