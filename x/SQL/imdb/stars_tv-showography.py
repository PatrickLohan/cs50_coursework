from cs50 import SQL

db = SQL("sqlite:///shows.db")
star = input("Name: ").strip()
sql = ("SELECT title,year FROM shows WHERE id IN"
       "(SELECT show_id FROM stars WHERE person_id IN"
       "(SELECT id FROM people WHERE name = ?))"
       "ORDER BY year")

rows = db.execute(sql, star)

for row in rows:
    print(row['title'], end=" ")
    print(row['year'])
