#include <stdio.h>
#include <cs50.h>

int get_pos_int(void);
void build_pyramid(int);

int main(void)
{
    // Ask user for number
    int height = get_pos_int();
    build_pyramid(height);
}

int get_pos_int(void)
{
    // initialize the return variable
    int n;

    do
    {
        // aske the user for input
        n = get_int("How high is your pyramid? ");
    }
    // discard non-positive non-integers and ask again
    while (n < 1 || n > 8);

    return n;
}

void build_pyramid(int height)
{
    // build each layer
    for (int i = 0; i < height; i++)
    {
        // build the left wall
        for (int j = height; j > 0; j--)
        {
            if (i >= j - 1)
            {
                printf("#");
            }
            else
            {
                printf(" ");
            }
        }

        // Create an atrium
        printf("  ");

        // build the right wall
        for (int k = 0; k < i + 1; k++)
        {
            printf("#");
        }
        printf("\n");
    }

}
