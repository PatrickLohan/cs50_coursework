#include <stdio.h>
#include <cs50.h>

// prototypes
long get_positive_long(void);

int main(void)
{
    // get valid user input
    long cc_number = get_positive_long(), remainder, result = 0;
    int counter = 0, first_digit = 0, second_digit; // for storing cc_number length

    // apply Luhn's algorithm
    do
    {
        counter++;
        remainder = cc_number % 10;

        // check for even/odd
        if (counter % 2 > 0)
        {
            result += remainder;
        }
        else
        {
            int local_remainder = remainder * 2;
            // split double digits and add each to total
            if (local_remainder > 9)
            {
                int a = local_remainder / 10;
                int b = local_remainder % 10;
                result += a + b;
            }
            else
            {
                result += local_remainder;
            }
        }
        // store first two cc digits and pop the last number off
        second_digit = first_digit;
        first_digit = cc_number;
        cc_number = cc_number / 10;
    }
    while (cc_number > 0);

    if (result % 10 == 0)
    {
        if ((counter == 13 || counter == 16) && first_digit == 4)
        {
            printf("VISA\n");
        }
        else if (counter == 15 && (second_digit == 34 || second_digit ==  37))
        {
            printf("AMEX\n");
        }
        else if (counter == 16 && (second_digit > 50 && second_digit < 56))
        {
            printf("MASTERCARD\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }
    else
    {
        printf("INVALID\n");
    }
    return 0;
}

long get_positive_long(void)
{
    long result;
    do
    {
        result = get_long("Number: ");
        return result;
    }
    while (result < 0);
}
