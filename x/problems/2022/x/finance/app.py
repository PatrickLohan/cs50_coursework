"""Ye olde stocs and bondes coynter."""
import os
import time
from datetime import datetime
import pprint

# Make sure API key is set
if not os.environ.get("API_KEY"):
    raise RuntimeError("API_KEY not set")
# from dotenv import load_dotenv

from cs50 import SQL

# from tempfile import mkdtemp
from werkzeug.security import check_password_hash, generate_password_hash

from flask import Flask, flash, redirect, render_template, request, session
from flask_session import Session
from flask.helpers import get_flashed_messages

from helpers import apology, login_required, lookup, usd

# load_dotenv()
# API_KEY = os.environ.get("API_KEY")


# Configure application
app = Flask(__name__)
# For flash messages
app.secret_key = os.environ.get("FLASH_KEY")

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Custom filter
app.jinja_env.filters["usd"] = usd

# configure session to use filesystem (instead of signed cookies)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")


@app.after_request
def after_request(response):
    """Ensure responses aren't cached."""
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/")
@login_required
def index():
    """Show portfolio of stocks."""

    # Check if user has stocks
    transactions_query: str = (
        "SELECT symbol, "
        "SUM(shares) AS shares "
        "FROM transactions "
        "WHERE user_id = ? "
        "GROUP BY symbol"
    )
    transactions_summed: [] = db.execute(transactions_query, session['user_id'])

    # Get user's cash
    sql: str = "SELECT cash FROM users WHERE id = ?"
    assets = db.execute(sql, session['user_id'])
    print(f"ASSETS SUMMED: {assets}")
    assets = assets[0]["cash"]

    # Check user has any stocks, and if so iterate
    if len(transactions_summed) > 0:
        for transaction in transactions_summed:
            transaction["price"] = lookup(transaction["symbol"])["price"]
            transaction["value"] = transaction["price"] * float(transaction["shares"])
            print(f"TRANSACTIONS SUMMED: {transactions_summed}")
            print(f"ASSETS SUMMED: {assets}")
            assets += transaction["value"]

    return render_template("index.html", stocks=transactions_summed, assets=assets)


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Allow user to buy an amount of stock from a symbol."""
    get_flashed_messages()

    if request.method == "POST":
        # Get the user's input
        stock = request.form.get("symbol")
        shares = request.form.get("shares")
        print(f"SHARES SELECTED: {shares}")

        # Check that both fields have input.
        # Flash an error message if not and keep user on buy page.
        if not stock or not shares:
            flash("Please enter something into both fields", "error")
            return redirect("/buy")

        # Make variables to hold an integer
        # shares = int(shares)
        stock = lookup(stock)
        # Check that a real symbol was used,
        # and a positive integer number of shares was requested.
        # if stock is None or shares <= 0:
        #     if stock is None:
        #         flash("We can't find that symbol.", "error")
        #     if shares <= 0:
        #         flash("Please enter a positive number.", "error")
        #     return redirect("/buy")
        if not stock["symbol"]:
            return apology("Symbol not found", "400")
        check_int = isinstance(shares, int)
        print(f">>> CHECK SHARES IS INT: {check_int}")
        if not check_int:
            return apology("Not positive integer", "400")

        # Now we know the user has entered a real symbol and a positive amount
        # of shares to buy. We'll multiply the stock price by the number of
        # shares to get a total.
        total_cost = stock["price"] * shares
        sql = "SELECT cash FROM users WHERE id = ?"
        value = session["user_id"]
        cash = db.execute(sql, value)[0]["cash"]

        if total_cost <= cash:

            # Update users table
            sql = "UPDATE users SET cash = cash - ? WHERE id = ?"
            user_id = session["user_id"]
            db.execute(sql, total_cost, user_id)

            # Update shares table
            sql = (
                "INSERT INTO transactions(symbol,shares,price,dt,user_id) "
                "VALUES (?,?,?,?,?)"
                )
            db.execute(
                sql,
                stock["symbol"],
                str(shares),
                str(stock["price"]),
                time.time(),
                user_id,
            )

            return render_template("index.html")

        return apology("Not enough cash!")

    # If GET request (that is, when initially loading or being redirected to)
    return render_template("buy.html")


@app.route("/history")
@login_required
def history():
    """Show history of transactions."""
    sql_trans = (
        "SELECT symbol,shares,price,dt FROM transactions "
        "WHERE user_id = ?"
        )
    trans = db.execute(sql_trans, session['user_id'])
    print(f"TRANSACTIONS HISTORY: {trans}")
    for tran in trans:
        if tran['shares'] < 0:
            tran['sold'] = abs(tran['shares'])
        else:
            tran['purchased'] = tran['shares']
        tran['dt'] = float(tran['dt'])
        tran['date'] = datetime.fromtimestamp(tran['dt']).strftime('%d-%m-%Y')
        tran['time'] = datetime.fromtimestamp(tran['dt']).strftime('%H:%M.%S')
        print(
            "DATE OF SALE/PURCHASE: "
            f"{tran['date']}"
            )
        print(
            "time OF SALE/PURCHASE: "
            f"{tran['time']}"
            )
    trans = sorted(trans, key=lambda d: d['dt'], reverse=True)
    pprint.pprint(trans)
    return render_template("history.html", trans=trans)


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in."""
    # Forget any user_id, but maintain flashed message if present. Source:
    # https://www.reddit.com/r/cs50/comments/fw50k5/message_flashing_not_working_with_redirect_to/
    if session.get("_flashes"):
        flashes = session.get("_flashes")
        session.clear()
        session["_flashes"] = flashes
    else:
        session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        username: str = request.form.get("username")
        password: str = request.form.get("password")

        # Ensure username and password was submitted
        if not username and not password:
            flash("Must provide a username and password", "error")
            return redirect("/login")
        # Ensure username was submitted
        if not username:
            return apology("must provide username", 400)
        # Ensure password was submitted
        if not password:
            return apology("must provide password", 400)

        # Query database for username
        sql: str = "SELECT * FROM users WHERE username = ?"
        rows: str = db.execute(sql, username)

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], password):  # noqa
            # return apology("invalid username and/or password", 403)
            flash("invalid username and/or password", "error")
            return redirect("/login")

        # Remember which user has logged in
        session["user_id"]: str = rows[0]["id"]

        # Redirect user to home page
        username: str = rows[0]["username"]
        flash(f"Welcome, {username}", "message")
        return redirect("/", "200")

    # User reached route via GET (as by clicking a link or via redirect)
    return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out."""
    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote and user's cash balance."""
    if request.method == "POST":
        res: dict = lookup(request.form.get("symbol"))
        print(f"RESULT: {res}")
        if res:
            # Get the current user's cash balance.
            # SQL returns a k/v in a 1 item list.
            return render_template("quoted.html", quote=res)

        return apology("Symbol does not exist.")

    return render_template("quote.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user."""
    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":
        username: str = request.form.get("username")
        password: str = request.form.get("password")
        confirmation: str = request.form.get("confirmation")

        # Ensure all fields were submitted
        if not username or not password or not confirmation:
            return apology("must provide username and password", 400)

        # ensure both password fields are the same
        if password != confirmation:
            return apology("Passwords do not match", 400)

        # check that username isn't taken
        sql: str = "SELECT username FROM users WHERE username = ?"
        rows = db.execute(sql, username)
        if len(rows) > 0:
            return apology("Your name is already taken", 400)

        # if all is good, add user to database and log them in
        password_hash: str = generate_password_hash(password)
        sql: str = "INSERT INTO users (username, hash) VALUES(?, ?)"
        rows = db.execute(sql, username, password_hash)

        # sql: str = "SELECT id FROM users WHERE username = ?"
        session["user_id"]: int = rows

        flash("You registered successfully! Please log in.")
        return redirect("/")

    # Show correct page on loading route
    return render_template("register.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """Sell shares of stock."""
    if request.method == "POST":

        symbol_selected: str = request.form.get("symbol_select")
        shares_selected: int = int(request.form.get("amount"))

        # Check both fields have input
        if not symbol_selected or not shares_selected or shares_selected < 1:
            flash("Please enter valid values", "warning")
            return redirect("sell")

        # Update user cash and insert entry into transactions
        sql_shares_held = (
            "SELECT shares FROM transactions "
            "WHERE user_id = ? AND symbol = ?"
            )
        shares_held = db.execute(
            sql_shares_held,
            session["user_id"],
            symbol_selected
            )[0]["shares"]

        # Warn user if more shares than held selected
        if shares_selected > shares_held:
            flash("You don't have that many shares to sell", "warning")
            return redirect("/")

        # Flip shares selected to negative for db call
        shares_selected = -abs(shares_selected)

        # Get current price of shares
        stock_price = lookup(symbol_selected)["price"]
        cash_rx = int(stock_price) * shares_selected

        # Add sale price to user's cash
        sql_update_user: str = "UPDATE users SET cash = cash + ? WHERE id = ?"  # noqa
        db.execute(sql_update_user, cash_rx, session["user_id"])

        # Insert data into transactions as new entry
        sql_insert_sale: str = (
            "INSERT INTO transactions(symbol,shares,price,dt,user_id) "
            "VALUES(?,?,?,?,?)"
        )
        db.execute(
            sql_insert_sale,
            symbol_selected,
            str(shares_selected),
            str(stock_price),
            str(int(time.time())),
            session["user_id"]
        )
        return redirect("/")

    # IF NOT POST
    sql: str = (
        "SELECT symbol,"
        "SUM(shares) AS shares "
        "FROM transactions "
        "WHERE user_id = ? "
        "GROUP BY symbol"
    )
    stocks = db.execute(sql, session["user_id"])
    print(f"STONKS: {stocks}")
    return render_template("sell.html", stocks=stocks)
