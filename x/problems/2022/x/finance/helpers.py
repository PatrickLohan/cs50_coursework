"""Docstring."""
import os
import requests
import urllib.parse

# from flask import redirect, render_template, request, session
from flask import redirect, render_template, session
from functools import wraps

from cs50 import SQL

db = SQL("sqlite:///finance.db")


def apology(message: str, code: int = 400):
    """Render message as an apology to user."""

    def escape(s: str) -> str:
        """
        Escape special characters.

        https://github.com/jacebrowning/memegen#special-characters
        """
        for old, new in [
            ("-", "--"),
            (" ", "-"),
            ("_", "__"),
            ("?", "~q"),
            ("%", "~p"),
            ("#", "~h"),
            ("/", "~s"),
            ('"', "''"),
        ]:
            s = s.replace(old, new)
        return s

    return render_template("apology.html", top=code, bottom=escape(message)), code


def login_required(f):
    """
    Decorate routes to require login.

    https://flask.palletsprojects.com/en/1.1.x/patterns/viewdecorators/
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)

    return decorated_function


def lookup(symbol: str):
    """Look up quote for symbol."""
    # Contact API
    try:
        api_key: str = os.environ.get("API_KEY")
        url: str = f"https://cloud.iexapis.com/stable/stock/{urllib.parse.quote_plus(symbol)}/quote?token={api_key}"
        response: str = requests.get(url)
        response.raise_for_status()
    except requests.RequestException:
        return None

    # Parse response
    try:
        quote: dict = response.json()
        return {
            "name": quote["companyName"],
            "price": float(quote["latestPrice"]),
            "symbol": quote["symbol"],
        }
    except (KeyError, TypeError, ValueError):
        return None


def usd(value: float) -> str:
    """Format value as USD."""
    return f"${value:,.2f}"
