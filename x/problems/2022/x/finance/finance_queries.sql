-- UPSERT SHARES
INSERT INTO shares(symbol,shares,bought_cost,user_id)
VALUES ("NFLX",5,1000,1)
ON CONFLICT(symbol,user_id)
DO UPDATE SET bought_cost=bought_cost+1000;
-- UPSERT USER
UPDATE users SET cash = cash - 1000 WHERE id = 1;
