#include <cs50.h>
#include <stdio.h>

// A program that outputs the amount of years it would take to reach a
// population size from a starting amount, both user inputted, given that the
// birth-rate is n/3 and mortality is n/4 
long start_pop(void);
long end_pop(long pop);

int main(void)
{
    // TODO: Prompt for start size
    long population = start_pop();

    // TODO: Prompt for end size
    long end_population = end_pop(population);

    // TODO: Calculate number of years until we reach threshold
    long years = 0;
    while (population < end_population)
    {
        long births = 0;
        long deaths = 0;
        births += (long)population / 3;
        deaths -= (long)population / 4;
        population += births + deaths;
        years++;
    }

    // TODO: Prlong number of years
    printf("Years: %li\n", years);
}

long start_pop(void)
{
    long pop;
    do
    {
        pop = get_long("Start size: \n");
    }
    while (pop < 9);

    return pop;
}

long end_pop(long start)
{
    long end;
    do
    {
        end = get_long("End size: \n");
    }
    while (end < start);

    return end;
}

