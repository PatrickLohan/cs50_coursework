#include <cs50.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
	string names[] = {
		"Alice",
		"Bob",
		"Charlie",
		"Doris",
		"Ethelbert",
		"Fiona",
		"Georgina",
		"Harriet"
	};

	for(int i = 0; i < 8; i++)
	{
		if(strcmp(names[i], "Zeus") == 0)
		{
			printf("Found\n");
			return 0;
		}
	}
	printf("Not found\n");
	return 1;
}
