#include <cs50.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
    string name;
    string number;
} person; // name of structure

int main(void)
{
    person people[3];

    people[0].name = "Apollo";
    people[0].number = "123";

    people[1].name = "Barney";
    people[1].number = "456";

    people[2].name = "Charlie";
    people[2].number = "789";

    string name = get_string("Name? ");
    for (int i = 0; i < 3; i++)
    {
        if (strcmp(people[i].name, name) == 0)
        {
            printf("%s's number is %s\n", name, people[i].number);
            return 0;
        }
    }
    printf("Not found\n");
    return 1;
}
