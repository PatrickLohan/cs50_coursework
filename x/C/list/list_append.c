#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>

// We need to talk about pointers
typedef struct Node
{
    int number;        // or whatever data is to be held
    struct Node *next; // pointer to next node, or end of list if NULL
} Node;

int main(int argc, char *argv[])
{
    Node *list = NULL;

    for(int i = 1; i < argc; i++)
    {
        // convert arg to int
        int number = atoi(argv[i]);

        Node *n = malloc(sizeof(Node));
        if(n == NULL)
        {
            return 1;
        }
        n->number = number; // next command line arg
        n->next = NULL;     // point to existing list address

        if(list == NULL)
        {
            list = n;
        }
        else
        {
            for(Node *ptr = list; ptr != NULL; ptr = ptr->next)
            {
                if(ptr->next == NULL)
                {
                    ptr->next = n;
                    break;
                }
            }
        }
    }
}