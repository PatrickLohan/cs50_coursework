#include <stdio.h>
#include <stdlib.h>

/* int main() */
/* { */
    /* int *list = malloc(3 * sizeof(int)); */

    /* if (list == NULL) */
    /* { */
    /*     return 1; */
    /* } */

    /* list[0] = 1; */
    /* list[1] = 2; */
    /* list[2] = 3; */

    /* int *tmp = realloc(list, 4 * sizeof(int)); */
    /* if (tmp == NULL) */
    /* { */
    /*     free(list); */
    /*     return 1; */
    /* } */

    /* tmp[3] = 4; */

    /* list = tmp; */

    /* for (int i = 0; i < 4; i++) */
    /* { */
    /*     printf("%i\n", list[i]); */
    /* } */


    /* free(list); */
    /* return 0; */
/* } */

typedef struct node
{
    int number; // or whatever data is to be held
    struct node *next; // pointer to next node, or end of list if NULL
}
node;

int main(int argc, char *argv[])
{
    //  Declare new linked list. It starts empty, so initialise with NULL
    node *list = NULL;


    // A new node (n) is allocated using malloc(). Memory allocation is important as it provides storage for our new
    // node. The allocated memory size is that of the node structure type. The allocated memory is then checked; if
    // malloc() failed to allocate space (which might happen if the system is out of memory), it returns NULL, and
    // hence the program must terminate by returning 1.
    node *n = malloc(sizeof(node));
    if(n == NULL)
    {
        return 1;
    }
    // The 'number' field of the new node (n) is given the value 1, and the next field is set to NULL, indicating the
    // end of the list (as it is the only node right now). This new node is then attached to our list.
    n->number = 1;
    n->next = NULL;
    list = n;


    // add the number 2 and 3 to the list. In each case, the new node is added at the end of the list, and the
    // next pointer of the previous last node is updated.
    n = malloc(sizeof(node));
    if(n == NULL)
    {
        free(list);
        return 1;
    }
    n->number = 2;  // -> is combined * and . syntax
    n->next = NULL;
    list->next = n;
    // second part
    n = malloc(sizeof(node));
    if(n == NULL)
    {
        // free in reverse order
        free(list->next);
        free(list);
        return 1;
    }
    n->number = 3;
    n->next = NULL;
    list->next->next = n;


    // Print linked list
    for (node *tmp = list; tmp != NULL; tmp = tmp->next)
    {
        printf("%i\n", tmp->number);
    }

    // Free list
    while (list != NULL)
    {
        node *tmp = list->next;
        free(list);
        list = tmp;
    }
    return 0;
}
