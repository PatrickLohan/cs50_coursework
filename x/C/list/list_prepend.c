#include <stdio.h>
#include <stdlib.h>

// We need to talk about pointers
typedef struct Node
{
    int number;        // or whatever data is to be held
    struct Node *next; // pointer to next node, or end of list if NULL
} Node;

int main(int argc, char *argv[])
{
    Node *list = NULL;

    for(int i = 1; i < argc; i++)
    {
        int number = atoi(argv[i]);

        Node *n = malloc(sizeof(Node));
        if(n == NULL)
        {
            return 1;
        }
        n->number = number; // next command line arg
        n->next = list;     // point to existing list address
        list = n;
    }

    Node *ptr = list; // make a new pointer to point at 'beginning' of list, so that the list can e.g. be printed.
    while(ptr != NULL)
    {
        printf("%i\n", ptr->number);
        ptr = ptr->next;
    }
    // Freeing the allocated memory
    Node *temp;
    while(list != NULL)
    {
        temp = list;
        list = list->next;
        free(temp);
    }

    return 0; // return 0 to indicate successful termination
}
