#include <stdio.h>

int main(void)
{
    // scanf with ints is fine
    /* int x; */
    /* printf("x: "); */
    /* scanf("%i", &x); */
    /* printf("x: %i\n", x); */

    // but with strings can be dangerous
    char *s;
    printf("s: ");
    scanf("%s", s);
    printf("s: %s\n", s);

}
