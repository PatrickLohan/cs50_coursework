#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main(void)
{
    clock_t start_time = clock(); // setup for benchmark
    string s = get_string("Input: ");
    printf("Output: ");
    // this version is nearly 100x slower than simple '\0' check, obviously
    // we can use int i, n because they are same data types
    // for(int i = 0, n = strlen(s); i < n; i++)
    for(int i = 0; i != '\0'; i++)
    {
        printf("%c", s[i]);
    }
    printf("\n");

    // benchmark
    double elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
    printf("Done in %f seconds\n", elapsed_time);
}
