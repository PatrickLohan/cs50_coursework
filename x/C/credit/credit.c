#include <cs50.h>
#include <stdio.h>

// prototypes
long get_positive_long(void);

int main(void)
{
    long cc_number = get_positive_long(); // get valid user input

    int counter = 0, first_digit = 0, second_digit; // for storing cc_number length

    // apply Luhn's algorithm
    do
    {
        counter++;
        long remainder = cc_number % 10;

        if (counter % 2 > 0)
        {
            result += remainder;
        }
        else
        {
            int local_remainder = remainder * 2;
            if (local_remainder > 9)
            {
                int a = local_remainder / 10;
                int b = local_remainder % 10;
                result += a + b;
            }
            else
            {
                result += local_remainder;
            }
        }
        second_digit = first_digit;
        first_digit = cc_number / 10;
        cc_number /= 10;
    } while (cc_number > 0);

    if (result % 10 == 0)
    {
        if ((counter == 13 || counter == 16) && first_digit == 4)
        {
            printf("VISA\n");
        }
        else if (counter == 15 && (second_digit == 34 || second_digit == 37))
        {
            printf("AMEX\n");
        }
        else if (counter == 16 && (second_digit > 50 && second_digit < 56))
        {
            printf("MASTERCARD\n");
        }
        else
        {
            printf("INVALID\n");
        }
    }
    else
    {
        printf("INVALID\n");
    }
    return 0;
}

long get_positive_long(void)
{
    long result;
    do
    {
        result = get_long("Number: ");
        if (result < 0)
        {
            printf("Invalid input.\n");
        }
    } while (result < 0);
    return result;
}
