#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int main(void)
{
    char *s = get_string("s: ");
    // if string too long for memory
    if(s == NULL)
    {
        return 1;
    }

    // this will only make another pointer to the address
    // so you will be working on the original not an actual copy of the value
    // char *t = s;

    // instead we need to do this to deep copy
    // allocate enough memory to hold the string being copied, plus one for the escape character
    char *t = malloc(strlen(s) + 1);
    // breakout if there is not enough memory for malloc to allocate
    if(t == NULL)
    {
        return 1;
    }
    // now copy using strcpy instead of a loop
    strcpy(t, s);

    if(strlen(t) > 0)
    {
        t[0] = toupper(t[0]);
    }

    printf("s: %s\n", s);
    printf("t: %s\n", t);

    free(t);
    return 0;
}
