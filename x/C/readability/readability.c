#include <cs50.h>
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

int count_letters( string text );
int count_words( string text );
int count_sentences( string text );
double calculate_colemanliau( int l, int w, int s );

int main( void )
{
    // get user input and calculate needed values
    string text = get_string( "Text: " );
    int letters = count_letters( text );
    int words = count_words( text );
    int sentences = count_sentences( text );

    // run the CL algorithm and print the correct grade
    double index = calculate_colemanliau( letters, words, sentences );
    if ( index > 16 )
    {
        printf( "Grade 16+\n" );
    }
    else if ( index < 1 )
    {
        printf( "Before Grade 1\n" );
    }
    else
    {
        printf( "Grade %i\n", (int)round( index ) );
    }
}

int count_letters( string text )
{
    int l = 0;
    for ( int i = 0, n = strlen( text ); i < n; i++ )
    {
        if ( tolower( text[i] ) >= 'a' && tolower( text[i] ) <= 'z' )
        {
            l += 1;
        }
    }
    return l;
}

int count_words( string text )
{
    int w = 0;
    for ( int i = 0, n = strlen( text ); i < n; i++ )
    {
        if ( text[i] == ' ' )
        {
            w += 1;
        }
    }
    if ( w > 0 )
    {
        w += 1;
    }
    return w;
}

int count_sentences( string text )
{
    int s = 0;
    for ( int i = 0, n = strlen( text ); i < n; i++ )
    {
        if ( text[i] == '.' || text[i] == '!' || text[i] == '?' )
        {
            s += 1;
        }
    }
    return s;
}

double calculate_colemanliau( int l, int w, int s )
{
    // index = 0.0588 * L - 0.296 * S - 15.8
    double lper = (double)l / w * 100;
    double sper = (double)s / w * 100;
    double result = 0.0588 * lper - 0.296 * sper - 15.8;
    return result;
}
