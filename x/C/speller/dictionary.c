// Implements a dictionary's functionality

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>

#include "dictionary.h"

// Represents a node in a hash table
typedef struct node
{
    char word[LENGTH + 1];
    struct node *next;
}
node;

// TODO: Choose number of buckets in hash table
//const unsigned int N = 26;
#define N 26

// Hash table
node *table[N];

int word_count = 0;

// Returns true if word is in dictionary, else false
bool check(const char *word)
{
    int hash_value = hash(word);

    // Create a cursor variable that traverses the hash table linked list
    node *cursor = table[hash_value];

    // iterate until either word found or end of list (NULL pointer)
    while(cursor != NULL)
    {
        if(strcasecmp(cursor->word, word) == 0)
        {
            return true;
        }
        cursor = cursor->next;
    }

    return false;
}

// Hashes word to a number
unsigned int hash(const char *word)
{
    // TODO: Improve this hash function
    return toupper(word[0]) - 'A';
}

// Loads dictionary into memory, returning true if successful, else false
bool load(const char *dictionary)
{
    // Open the dictionary for reading
    FILE *dictionary_file = fopen(dictionary, "r");
    if(dictionary_file == NULL)
    {
        return false;
    }

    // temp buffer between file and hash table
    char tmp_word[LENGTH + 1];

    // Read into hash table
    while(fscanf(dictionary_file, "%s", tmp_word) != EOF)
    {
        // for each word in file create a new node
        node *tmp_node = malloc(sizeof(node));
        if(tmp_node == NULL)
        {
            return false;
        }

        // increment word count
        word_count++;

        // Copy word into node
        strcpy(tmp_node->word, tmp_word);
        tmp_node->next = NULL;

        // obtain hashing index
        int hash_index = hash(tmp_word);

        // set up temporary node with temporary word
        if(table[hash_index] == NULL)
        {
            tmp_node->next = NULL;
        }
        else
        {
            tmp_node->next = table[hash_index];
        }
        table[hash_index] = tmp_node;
    }
    fclose(dictionary_file);

    return true;
}

// Returns number of words in dictionary if loaded, else 0 if not yet loaded
unsigned int size(void)
{
    return word_count;
}

// Unloads dictionary from memory, returning true if successful, else false
bool unload(void)
{

    for(int i = 0; i < N; i++)
    {
        node *head = table[i];
        node *cursor = head;
        node *tmp = cursor;
        while(cursor != NULL)
        {
            cursor = cursor->next;
            free(tmp);
            tmp = cursor;
        }
    }
    return true;
}
