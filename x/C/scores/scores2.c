#include <cs50.h>
#include <stdio.h>

const int N = 3; // set a constant for the amount of scores
float average_score(int length, int array[]); // define prototype

int main(void)
{
    int scores[N]; // create an array to hold scores, size N

    // iterate a loop of size N
    for (int i = 0; i < N; i++)
    {
        scores[i] = get_int("Score: "); // gather and store N scores
    }
    printf("Result: %.2f\n", average_score(N, scores)); // show the average score
}

float average_score(int length, int arr[])
{
    int sum = 0; // declare a counter and start at 0

    // iterate over the array of scores and add each to sum
    for (int i = 0; i < length; i++)
    {
        sum += arr[i];
    }

    // return the average
    return sum / (float)length;
}
