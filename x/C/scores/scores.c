#include <cs50.h>
#include <stdio.h>

int main(void)
{
    const int N = 3;
    int scores[N];
    int count = 0;
    for(int i = 0; i < N; i++)
    {
        scores[i] = get_int("Score: ");
        count += scores[i];
    }
    float result = count / (float) N;
    printf("Result: %.2f\n", result);
}
