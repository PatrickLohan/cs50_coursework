#include <cs50.h>
#include <stdio.h>
#include <unistd.h> // needed for the sleep function

// Below is a 'hint' at a function that is later defined.
// We call this a 'prototype'
void countdown(int n);
int get_pos_int(void);

int main(void)
{
    string username = get_string("What is your name? ");
    printf("Greetings, %s\n", username);

    char confirm = get_char("Are you ready for a space adventure? ");
    if (confirm == 'y' || confirm == 'Y')
    {
        countdown(3);
    }
    else
    {
        printf("Maybe next time!\n");
    }

    printf("Your number is %i\n", get_pos_int());
}

void countdown(int n)
{
    for (int i = n; i > 0; i--)
    {
        printf("Countdown: %i\n", i);
        sleep(1);
    }
    printf("0!!! BLAST OFF!!!\n");
}

int get_pos_int(void)
{
    int n;

    do
    {
        n = get_int("Be happy and give me a positive number: ");
    } while (n < 1);

    return n;
}
