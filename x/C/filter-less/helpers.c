#include "helpers.h"
#include <stdio.h>
#include <math.h>

// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            // get the original values
            float blue = image[i][j].rgbtBlue;
            float green = image[i][j].rgbtGreen;
            float red = image[i][j].rgbtRed;

            // calculate the new values
            int average_value = round((blue + green + red) / 3);


            image[i][j].rgbtBlue = image[i][j].rgbtGreen =  image[i][j].rgbtRed = average_value;
        }
    }
    return;
}

// Convert image to sepia
void sepia(int height, int width, RGBTRIPLE image[height][width])
{
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            float blue,green,red;

            blue =
                (image[i][j].rgbtRed * 0.272) +
                (image[i][j].rgbtGreen * 0.534) +
                (image[i][j].rgbtBlue * 0.131);
            if(blue > 255)
            {
                blue = 255;
            }

            green =
                (image[i][j].rgbtRed * 0.349) +
                (image[i][j].rgbtGreen * 0.686) +
                (image[i][j].rgbtBlue * 0.168);
            if(green > 255)
            {
                green = 255;
            }

            red =
                (image[i][j].rgbtRed * 0.393) +
                (image[i][j].rgbtGreen * 0.769) +
                (image[i][j].rgbtBlue * 0.189);
            if(red > 255)
            {
                red = 255;
            }

            image[i][j].rgbtBlue = round(blue);
            image[i][j].rgbtGreen = round(green);
            image[i][j].rgbtRed = round(red);

        }
    }
    return;
}

// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width/2; j++)
        {
            RGBTRIPLE temp = image[i][j];
            image[i][j] = image[i][width - (j + 1)];
            image[i][width - (j + 1)] = temp;
        }
    }
    return;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    // copy image to temp 2d array
    RGBTRIPLE temp[height][width];
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            temp[i][j] = image[i][j];
        }
    }

    // loop over each row of temp
    for(int i = 0; i < height; i++)
    {
        // loop over each pixel in row
        for(int j = 0; j < width; j++)
        {
            // declare counters for amount of pixels adjacent and sum of each r g b
            int sumBlue,sumGreen,sumRed;
            float adjPxls;
            sumBlue = sumGreen = sumRed = adjPxls = 0;

            // for each pixel in row check adjacent pixel exists
            // rows
            for(int y = -1; y < 2; y++)
            {
                // columns
                for(int x = -1; x < 2; x++)
                {
                    // check if each adj exists
                    if(i + y < 0 || i + y >= height)
                    {
                        continue;
                    }
                    if(j + x < 0 || j + x >= width)
                    {
                        continue;
                    }
                    // if exists add 1 to adjacent counter
                    adjPxls++;
                    // and add rgb values to sum counter
                    sumBlue += temp[i + y][j + x].rgbtBlue;
                    sumGreen += temp[i + y][j + x].rgbtGreen;
                    sumRed += temp[i + y][j + x].rgbtRed;
                }
            }

            // assign these new rgb values to each pixel in original
            image[i][j].rgbtBlue = round(sumBlue / adjPxls);
            image[i][j].rgbtGreen = round(sumGreen / adjPxls);
            image[i][j].rgbtRed = round(sumRed / adjPxls);
        }
    }

  return;
}
