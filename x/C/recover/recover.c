#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t BYTE;

int main(int argc, char *argv[])
{
    // check only 1 argument passed
    if(argc != 2)
    {
        printf("Usage: ./recover IMAGE\n");
        return 1;
    }
    // check argument is name of file and can be opened for reading
    /* char *input_file = argv[1]; */
    FILE *input_ptr = fopen(argv[1], "r");

    if (input_ptr == NULL) {
      printf("Error: cannot open %s\n", argv[1]);
      return 2;
    }

    // make a buffer for each 512 byte chunk of data
    BYTE read_buffer[512];
    // start the new file name counter
    int count = 0;
    // and create a pointer for the new files
    FILE *image_ptr = NULL;
    char filename[8];



    // Keep examining each 512 byte chunk until the end of the input file
    while(fread(&read_buffer, 512, 1, input_ptr) == 1)
    {

        // Check for magic bytes (0xff 0xd8 0xff 0xe0...0xef):
        if(
           read_buffer[0] == 0xff &&
           read_buffer[1] == 0xd8 &&
           read_buffer[2] == 0xff &&
           (read_buffer[3] & 0xf0) == 0xe0
           )
        {
            // If not first image, close previous file
            if(!(count == 0))
            {
                fclose(image_ptr);
            }
            // Create a file to hold data
            sprintf(filename, "%03i.jpg", count);
            image_ptr = fopen(filename, "w");
            count++;
        }

        // If JPEG has been found, write to file
        if (!(count == 0))
        {
            fwrite(&read_buffer, 512, 1, image_ptr);
        }
    }

    // Tidy up before quitting
    fclose(input_ptr);
    fclose(image_ptr);
    return 0;

}
