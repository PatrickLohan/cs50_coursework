#include <cs50.h>
#include <stdio.h>

int main(void)
{
    /* int length = 0; */
    /* int height = 0; */
    /* while (length < 1) */
    /* { */
    /*     length = get_int("Length: "); */
    /* } */
    /* while (height < 1) */
    /* { */
    /*     height = get_int("Height: "); */
    /* } */
    int length;
    do
    {
        length = get_int("Length: ");
    } while (length < 1);

    int height;
    do
    {
        height = get_int("Height: ");
    } while (height < 1);

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < length; j++)
        {
            printf("#");
        }
        printf("\n");
    }
}
