#include <cs50.h>
#include <string.h> // strlen
#include <ctype.h> // isdigit
#include <stdlib.h> // exit and atoi
#include <stdio.h>

string show_usage_and_exit(void);
bool check_is_number(char[]);

int main(int argc, string argv[])
{
	// check for only one argument, and that it is an integer
    if (argc == 2 && check_is_number(argv[1]))
    {
		// convert the argument to an integer
        int k = atoi(argv[1]);

		// Ask user for plaintext
        string ptxt = get_string("plaintext: ");
		// show a placeholder start
        printf("ciphertext: ");

		// iterate through plain text letter by letter
        for (int i = 0, n = strlen(ptxt) ; i < n; i++)
        {
            // check if lowercase
            if (ptxt[i] >= 'a' && ptxt[i] <= 'z')
            {
				// print the converted letter
                printf("%c", (((ptxt[i] - 'a') + k) % 26) + 'a'); // print out lowercase with key
            } // if it it between uppercase A and C
            else if (ptxt[i] >= 'A' && ptxt[i] <= 'Z')
            {
                printf("%c", (((ptxt[i] - 'A') + k) % 26) + 'A'); // print out uppercase with key
            }
            else
            {
                printf("%c", ptxt[i]);
            }
        }
        printf("\n");
        return 0;
    }
    else
    {
      show_usage_and_exit();
    }
}

string show_usage_and_exit(void)
{
  printf("Usage: ./caesar key\n");
  exit(1);
}

bool check_is_number(string arg_passed)
{
	for(int i = 0, n = strlen(arg_passed); i < n; i++)
	{
		if(!isdigit(arg_passed[i]))
		{
			show_usage_and_exit();
		}
	}
	return true;
}
