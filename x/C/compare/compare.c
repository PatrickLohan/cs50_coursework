#include <cs50.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
    int i = get_int("int i: ");
    int j = get_int("int j: ");

    if (i == j) {
        printf("Same!\n");
    }
    else
    {
        printf("Different!\n");
    }

    // strings are an abstraction to char *
    // that means we can't simply compare multiple strings for equality as they
    // will be compared based on their addresses (char *)!
    // however we can use strcmp like this (which is why we are including string.h):
    char *s = "Boop!";
    char *t = get_string("Guess the word: ");

    if (strcmp(s, t) == 0)

      {
        printf("Same!\n");
    }
    else
    {
        printf("different!\n");
    }
    printf("Address of first char in s: %p\n", s);
    printf("Address of first char in t: %p\n", t);
}
