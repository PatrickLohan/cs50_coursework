#include <cs50.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
    string s = get_string("Input: ");
    printf("Output: ");

    for (int i = 0, n = strlen(s); i < n; i++)
    {
        // string.h version
        /* if (s[i] >= 'a' && s[i] <= 'z') */
        /* { */
        /*     printf("%c", s[i] - 32); */
        /* } */
        /* else */
        /* { */
        /*     printf("%c", s[i]); */
        /* } */

        // ctype.h version
        printf("%c", toupper(s[i])); // toupper expects a char
    }
    printf("\n");
}
