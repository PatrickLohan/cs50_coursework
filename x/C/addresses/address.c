/* #include <stdio.h> */

/* int main(void) */
/* { */
/* 	// declare variable n with value of 50 */
/*     int n = 50; */
/* 	// declare a pointer p that points to the address of n */
/*     int *p = &n; */
/* 	// print out the value at an address (e.g. *p) */
/* 	printf("Value from pointer: %i\n", *p); */
/*     // and this does the same as above, but accessing the */
/*     //variable address directly */
/*     printf("Value using pointer format code: %p\n", &n); */
/*     // print out the address of the pointer p, using */
/* 	// the format code %p */
/* 	printf("Address: %p\n", p); */
/* } */

#include <stdio.h>

int main(void)
{
    char *s = "HI!";
    // printf("%s\n", s);

    /* // the addresses will be the same if we use pointers */
    /* // c looks for the null pointer in arrays (\0) in order to know where the */
    /* // end of the array is! */
    /* char *p = &s[0]; */
    /* printf("%p\n", p); */
    /* printf("%p\n", s); */

    // A copy of a variable, or part thereof
    /* char c = s[0]; */
    /* char *p = &c; */
    /* printf("%p\n", s); */
    /* printf("%p\n", p); */

    // s is the address of the first char in a string (array of chars)
    // we can dereference using *s syntax to print only the first char in the array, e.g.
    // printf("%c\n", *s);
    printf("%c\n", *s);
    // weird stuff: printf("%c\n", s[-2]);

    // pointer arithmetic
    printf("%c\n", *(s + 2));
    // can be used on int data too, even though technically an int is 32bits (4 bytes)
    // the compiler will know that you mean x int
    // also, an array name can be treated as the address of the 0th element of the array
    int numbers[] = {1, 2, 3};
    printf("%i\n", *(numbers + 2));
}
