#include <stdio.h>

int main(void)
{
    int foo = 50;
    int *bar = &foo; // declare a pointer (bar) to an integer variable (foo)
    printf("%i\n", *bar); // print the integer that is at the address 'bar'
}

// a pointer is a variable that holds the address of some value/variable
// & == 'address of' operator
// * == dereference operator (take me to an address)
// %p == format code to print an address
