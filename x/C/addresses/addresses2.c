#include <stdio.h>

int main(void)
{
    char *s = "HI!";
    /* printf("%s\n", s); */
    /* printf("%c\n", s[0]); */
    /* printf("%c\n", s[1]); */
    /* printf("%c\n", s[2]); */

    // pointer arithmetic
    printf("%c", *s);
    printf("%c", *(s + 1));
    printf("%c\n", *(s + 2));
}

// double quotes ("") around a sequence of characters (i.e. a string) is seen by
// the compiler, and it will automatically point the pointer at the first
// char in the string
