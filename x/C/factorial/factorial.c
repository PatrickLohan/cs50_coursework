#include <cs50.h>
#include <stdio.h>

long factorial(int n);

int main(void)
{
    int f;
    do
    {
        f = get_int("Factorial? ");
    } while(f < 1 || f > 20);

    printf("%li\n", factorial(f));
}

long factorial(int n)
{
    if(n == 1)
    {
        return 1;
    }
    return n * factorial(n - 1);
}
