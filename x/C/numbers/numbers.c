#include <cs50.h>
#include <stdio.h>

int main(void)
{
	// use hitchcocks to create dynamic array
	int numbers[] = {7,8,9,2,3,4,5,6,0};

	for(int i = 0; i < 9; i++)
	{
		if(numbers[i] == 1)
		{
			printf("Found\n");
			return 0;
		}
	}
	printf("Not found\n");
	return 1;
}
