"""Froshims."""
from flask import Flask, render_template, request

app = Flask(__name__)

REGISTRANTS = {}

ACTS = [
    "Permaculture",
    "Veganism",
    "Free Love"
    ]


@app.route("/")
def index():
    """Gee, it's the homepage."""
    return render_template("index.html", acts=ACTS)


@app.route("/register", methods=["POST"])
def register():
    """Register."""
    # Validate submission
    if not request.form.get("name") or request.form.get("act") not in ACTS:
        return render_template("failure.html")
    return render_template("success.html")
