"""Ye olde stocs and bondes coynter."""

import os
import time
from datetime import datetime

from cs50 import SQL
from flask import Flask, flash, redirect, render_template, request, session
from werkzeug.security import check_password_hash, generate_password_hash

from flask_session import Session
from helpers import apology, login_required, lookup, usd

app = Flask(__name__)
# For flash messages
app.secret_key = os.environ.get("FLASH_KEY")

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Custom filter
app.jinja_env.filters["usd"] = usd

# configure session to use filesystem (instead of signed cookies)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///finance.db")


@app.after_request
def after_request(response):
    """Ensure responses aren't cached."""
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response


@app.route("/")
@login_required
def index():
    """Show portfolio of stocks."""
    user_id = session["user_id"]

    # Check if user has stocks
    transactions_query: str = (
        "SELECT symbol, "
        "SUM(shares) AS shares "
        "FROM transactions "
        "WHERE user_id = ? "
        "GROUP BY symbol"
    )
    transactions_summed: [] = db.execute(transactions_query, (user_id,))

    # Get user's cash
    sql: str = "SELECT cash FROM users WHERE id = ?"
    result = db.execute(sql, (user_id,))
    cash = 0
    if result:
        cash = result[0]["cash"]

    assets = 0
    # Check user has any stocks, and if so iterate
    if len(transactions_summed) > 0:
        for transaction in transactions_summed:
            transaction["price"] = lookup(transaction["symbol"])["price"]
            transaction["value"] = transaction["price"] * float(
                transaction["shares"]
            )  # noqa:E501
            print(f"TRANSACTIONS SUMMED: {transactions_summed}")
            print(f"ASSETS SUMMED: {assets}")
            print(f"CASH: {cash}")
            assets += transaction["value"]

    return render_template(
        "index.html", stocks=transactions_summed, assets=assets, cash=cash
    )  # noqa:E501


@app.route("/buy", methods=["GET", "POST"])
@login_required
def buy():
    """Allow user to buy an amount of stock from a symbol."""
    if request.method == "POST":
        # Get the user's input
        symbol = request.form.get("symbol")
        shares = request.form.get("shares")

        # Check that both fields have input
        if not symbol or not shares:
            return apology("Please enter a symbol and number of shares")

        # Validate the number of shares
        try:
            shares = int(shares)
            if shares <= 0:
                return apology("Number of shares must be a positive integer")
        except ValueError:
            return apology("Number of shares must be a positive integer")

        # Look up the stock symbol
        stock = lookup(symbol)
        if stock is None:
            return apology("Invalid stock symbol")

        # Calculate the total cost of the purchase
        total_cost = stock["price"] * shares

        # Get the user's current cash balance
        user_id = session["user_id"]
        cash_query = "SELECT cash FROM users WHERE id = ?"
        cash_result = db.execute(cash_query, (user_id,))
        cash = cash_result[0]["cash"]

        # Check if the user has enough cash
        if cash < total_cost:
            return apology("Insufficient funds")

        # Update the user's cash balance
        new_cash = cash - total_cost
        update_cash_query = "UPDATE users SET cash = ? WHERE id = ?"
        db.execute(update_cash_query, new_cash, user_id)

        # Insert the transaction into the transactions table
        insert_transaction_query = """
            INSERT INTO transactions (symbol, shares, price,     if result:
        assets = result[0]["cash"]
    else:
        assets = 0
dt, user_id)
            VALUES (?, ?, ?, ?, ?)
        """
        db.execute(
            "INSERT INTO transactions (symbol, shares, price, dt, user_id) VALUES (?, ?, ?, ?, ?)",
            symbol,
            shares,
            stock["price"],
            datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            user_id,
        )

        # Redirect the user to the home page
        return redirect("/")

    # If the request method is GET, render the buy page
    return render_template("buy.html")


@app.route("/history")
@login_required
def history():
    """Show history of transactions."""
    sql_query = "SELECT * FROM transactions WHERE user_id = ?"
    transactions = db.execute(sql_query, session["user_id"])
    for transaction in transactions:
        print(transaction)

    return render_template("history.html", transactions=transactions)


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in."""
    # Forget any user_id, but maintain flashed message if present. Source:
    # https://www.reddit.com/r/cs50/comments/fw50k5/message_flashing_not_working_with_redirect_to/
    if session.get("_flashes"):
        flashes = session.get("_flashes")
        session.clear()
        session["_flashes"] = flashes
    else:
        session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":
        username: str = request.form.get("username")
        password: str = request.form.get("password")

        # Ensure username and password was submitted
        if not username and not password:
            flash("Must provide a username and password", "error")
            return redirect("/login")
        # Ensure username was submitted
        if not username:
            # return apology("must provide username", 403)
            flash("Must provide username", "error")
            return redirect("/login")
        # Ensure password was submitted
        if not password:
            # return apology("must provide password", 403)
            flash("Must provide password", "error")
            return redirect("/login")

        # Query database for username
        sql: str = "SELECT * FROM users WHERE username = ?"
        rows: str = db.execute(sql, username)

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], password):  # noqa
            # return apology("invalid username and/or password", 403)
            flash("invalid username and/or password", "error")
            return redirect("/login")

        # Remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # Redirect user to home page
        username: str = rows[0]["username"]
        flash(f"Welcome, {username}", "message")
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out."""
    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")


@app.route("/quote", methods=["GET", "POST"])
@login_required
def quote():
    """Get stock quote and user's cash balance."""
    if request.method == "POST":
        res: dict = lookup(request.form.get("symbol"))
        print(f"RESULT: {res}")
        if res:
            # Get the current user's cash balance.
            # SQL returns a k/v in a 1 item list.
            return render_template("quoted.html", quote=res)

        return apology("Symbol does not exist.", 400)

    return render_template("quote.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user."""
    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":
        name: str = request.form.get("username")
        password_a: str = request.form.get("password")
        password_b: str = request.form.get("confirmation")

        # Ensure all fields were submitted
        if not name or not password_a or not password_b:
            return apology("must provide username and password", 400)

        # ensure both password fields are the same
        if password_a != password_b:
            return apology("Passwords do not match", 400)

        # check that username isn't taken
        sql: str = "SELECT id FROM users WHERE username = ?"
        if db.execute(sql, name):
            return apology("Your name is already taken", 400)

        # if all is good, add user to database and log them in
        hash_password: str = generate_password_hash(password_a)

        sql: str = "INSERT INTO users (username, hash) VALUES(?, ?)"
        db.execute(sql, name, hash_password)

        sql: str = "SELECT id FROM users WHERE username = ?"
        response = db.execute(sql, (name,))
        if response:
            session["user_id"] = response[0]["id"]

        return redirect("/")

    # Show correct page on loading route
    return render_template("register.html")


@app.route("/sell", methods=["GET", "POST"])
@login_required
def sell():
    """
    Allow a user to
    sell shares of a stock (that he or she owns).
    """

    if request.method == "GET":
        sql: str = (
            "SELECT symbol,"
            "SUM(shares) AS shares "
            "FROM transactions "
            "WHERE user_id = ? "
            "GROUP BY symbol"
        )
        stocks = db.execute(sql, session["user_id"])
        print(f"STONKS: {stocks}")
        return render_template("sell.html", stocks=stocks)
    # Require that a user input a stock’s symbol, implemented as a select menu
    # whose name is symbol. Render an apology if the user fails to select a stock
    # or if (somehow, once submitted) the user does not own any shares of that stock.
    if request.method == "POST":
        symbol: str = request.form.get("symbol")
        pending_shares_to_sell: int = int(request.form.get("shares"))

        if type(pending_shares_to_sell) is not int or pending_shares_to_sell < 1:
            return apology("Please use only positive numbers", 400)
        # Check both fields have input
        if not symbol or not pending_shares_to_sell or pending_shares_to_sell < 1:
            return apology("Please enter valid values", 400)

        # Submit the user’s input via POST to /sell.
        # Update user cash and insert entry into transactions
        sql_shares_held = """
        SELECT SUM(shares) as symbol_shares FROM transactions
        WHERE user_id = ?
          AND symbol = ?
          """
        shares_held = db.execute(sql_shares_held, session["user_id"], symbol)[0][
            "symbol_shares"
        ]

        # Warn user if more shares than held selected
        if pending_shares_to_sell > shares_held:
            return apology("Please enter valid values", 400)

        # Get current price of shares
        stock_price = lookup(symbol)["price"]
        cash_rx = stock_price * pending_shares_to_sell

        # Add sale price to user's cash
        sql_update_user: str = "UPDATE users SET cash = cash + ? WHERE id = ?"  # noqa
        db.execute(sql_update_user, cash_rx, session["user_id"])

        # Flip shares selected to negative for db call
        pending_shares_to_sell = -abs(pending_shares_to_sell)
        # Insert data into transactions as new entry
        sql_insert_sale: str = (
            "INSERT INTO transactions(symbol,shares,price,dt,user_id) "
            "VALUES(?,?,?,?,?)"
        )
        db.execute(
            sql_insert_sale,
            symbol,
            str(pending_shares_to_sell),
            str(stock_price),
            str(int(time.time())),
            session["user_id"],
        )

        # Upon completion, redirect the user to the home page.
        return redirect("/")
