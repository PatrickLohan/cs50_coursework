"""Simple flask app."""
from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    """homepage."""
    return render_template('index.html')


@app.route('/greet', methods=["POST"])
def greet():
    """hello."""
    name = request.form.get("name", "world")
    return render_template('greet.html', name=name)
