from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse


class NewTodoForm(forms.Form):
    title = forms.CharField(label="New todo")
    description = forms.CharField(label="Description")
    priority = forms.IntegerField(label="Priority", min_value=1, max_value=10)




def index(request):
    if "todos" not in request.session:
        request.session["todos"] = []

    return render(request, 'todos/index.html', {
        "todos": request.session["todos"]
    })

def add_todo(request):
    if request.method == "POST":
        form = NewTodoForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data["title"]
            description = form.cleaned_data["description"]
            priority = form.cleaned_data["priority"]
            request.session["todos"] += [{
                "title": title, "description": description, "priority": priority
            }]
            return HttpResponseRedirect(reverse('todos:index'))
        else:
            return render(request, 'todos/add-todo.html', {
                "form": form
            })

    return render(request, 'todos/add-todo.html',{
        "form": NewTodoForm()
    })

