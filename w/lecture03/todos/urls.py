from django.urls import path
from . import views

app_name = "todos"  # namespacing so we can use `url 'todos:index'` in a.href
urlpatterns = [
    path("", views.index, name="index"),
    path("add-todo/", views.add_todo, name="add"),
]