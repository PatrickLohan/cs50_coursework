from django.shortcuts import render
from datetime import datetime


def index(request):
    midnight = datetime.now().hour < 1 and datetime.now().minute < 20
    print(datetime.now().time())
    return render(request,
                  "timenow/index.html",
                  {
                      "date": datetime.now().date(),
                      "time": datetime.now().time(),
                      "midnight": midnight,
                  })