from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, "hello/index.html")

def greet(request, name):
    return render(request,
                  "hello/greet.html",
                  {
                      "name": name.capitalize(),
                  })

def test(request):
    return HttpResponse(f"""
        <html>
        <body>
        <h1>Hello, world!</h1>
        <p>Hello, tester!</p>
        </body>
        </html>
    """)
